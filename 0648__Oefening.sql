USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DemonstrateHandlerOrder`()
BEGIN
	declare random tinyint default 0;

    declare exit handler for sqlstate '45002'
    begin
		select 'State 45002 opgevangen, Geen probleem';
    end;
	
    -- Alle andere signalen worden op deze manier afgehandelt.
    declare continue handler for sqlexception
    begin
		select 'Een algemene fout opgevangen';
	end;
    
	-- floor doet alle comma's weg. Zonder de één gaat het van null tot 2 met die één van 1 tot 3
    set random = floor(rand() * 3) + 1;
    
    if random = 1 then
		signal sqlstate '45001';
	elseif random = 2 then
		signal sqlstate '45002';
	elseif random = 3 then
		signal sqlstate '45003';
	end if;
END$$

DELIMITER ;