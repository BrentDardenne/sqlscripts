USE `aptunes`;
DROP procedure IF EXISTS `cleanupOldMemberships`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `cleanupOldMemberships`(in enddate date, out numberCleaned int)
BEGIN

	start transaction;
	-- teller
	SELECT count(*) into numberCleaned FROM Lidmaatschappen 
	where Einddatum IS NOT NULL AND Einddatum < enddate;

	-- verwijderen 
	set sql_safe_updates = 0;
	DELETE FROM Lidmaatschappen
	where Einddatum IS NOT NULL AND Einddatum <  enddate;
	set sql_safe_updates = 1;
    commit;
END$$

DELIMITER ;


CALL CleanupOldMemberships('1980-01-01', @numberCleaned);
SELECT @numberCleaned;