-- Vraag 1 0640
use aptunes;

USE `aptunes`;
DROP procedure IF EXISTS `Getliedjes`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `Getliedjes` (IN nameliedje varchar(50))
BEGIN
	select
    Titel
    from liedjes
    where Titel like concat('%', nameliedje, '%');		
END$$

DELIMITER ;

call Getliedjes('web');