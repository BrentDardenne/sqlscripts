-- Oefening 0637
use ModernWays;

-- creer een nieuwe tabel met de auteursgegevens
-- Voornaam en Familienaam
use ModernWays;
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);

-- We vullen rijen Voornaam, Familienaam in Personen. We gebruiken distinct om all dubble waarden eruit te halen
-- Hiervoor gebruiken we een subquery
insert into Personen (Voornaam, Familienaam) 
	select distinct Voornaam, Familienaam from boeken;

-- tabel Personen uitbreiden met informatie over de personen
alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);


-- Foreign key om te verwijzen naar de auteur toevoegen
-- verwijzing zou beter not null zijn
-- op dit moment niet mogelijk, want de kolom moet ingevuld worden
-- op basis van vergelijking tussen Boekne en Personen
alter table Boeken add Personen_Id int null;

-- eerste zetten we sql_safe_updates op null om te kunne updaten dit is voor veiligheidsrenden. Maar als je man bent of lui zoals mij hou je het op null 
-- Dan voeren we de update script uit. Dit gaat in Personen_id de waarden van de Boeken.Personen_id invullen.alter.
-- alsook zal hij dit doen wanneer Boeken.Voornaam hetzelfde is als Personen.Voornaam en hetzelfde voor familienaam
-- Daarna zetten we sql_safe_updates terug op één. Dit voor veilig heids reden.
-- kopieer in de geldige combin	atie Boeken/ Personen
-- Het ID van de persoon
-- m.a.w. zorg ervoor dat een boek verwijst naar zijn auteur.
set sql_safe_updates = 0;
update Boeken cross join Personen
    set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;
set sql_safe_updates = 1;

-- Nu mogen Id's verplicht not null worden
-- foreign key verplicht maken
alter table Boeken change Personen_Id Personen_Id int not null;

-- overbodige kolommen mogen weg
alter table Boeken drop column Voornaam,
    drop column Familienaam;
    
-- dan de constraint toevoegen
--
alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);
   
   



    







