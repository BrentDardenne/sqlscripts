USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleaseLoop`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumReleaseLoop`(in extraRelease int)
BEGIN
	declare counter int default 0;
    declare success bool;
    createAlbumBand: loop
    call MockAlbumReleaseWithSuccess(success);
    if success = 1 then
		set counter = counter + 1;
	end if;
	if counter >= extraRelease then
    leave createAlbumBand;
    end if;
    end loop;
END$$

DELIMITER ;


