USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration2`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetAlbumDuration2`(in nrAlbum int,out totalDuration smallint unsigned )
sql security invoker -- dus hier vind een insert plaats de ap user mag geen insert doen mag de ap user de sort  produre niet gebruiken.
BEGIN
    -- de nummer die controleert
    declare ok integer default 0;
    -- variabelen
	declare songDuration tinyint unsigned;
    -- cursor stuk
	declare albumLus cursor for select Lengte from liedjes where nrAlbum = Albums_id;
    -- controle we gebruiken continue omdat we wel willen nakijken maar niet stoppen eens gecontroleert of er nog een waarde is gaan we voort.
    declare continue handler for not found set ok = 1;
    
    set totalDuration = 0;
    -- open cursor
    open albumLus;
    -- start lus
    getAlbum: loop
    -- fetch gedeelde
    fetch albumLus into songDuration;
    if ok = 1
		then
		leave getAlbum;
        end if;
        -- om te kijken of het iets terug geeft.
        -- select lijstAlbumTime;
		SET totalDuration = totalDuration + songDuration;
	
		-- eindig lus
		end loop getAlbum;
    
    -- sluit cursor
    close albumLus;   
END$$

DELIMITER ;