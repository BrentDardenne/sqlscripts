USE `aptunes`;
DROP procedure IF EXISTS `CreateAndReleaseAlbum`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateAndReleaseAlbum`(in newTitel varchar(100), in newBands_Id int)
BEGIN
	start transaction;
    insert into Albums (titel) values (newTitel);
	insert into Albumreleases (Bands_id, albums_id) values (newBands_Id, last_insert_id());
    commit;
END$$

DELIMITER ;

call CreateAndReleaseAlbum('weed',995);
