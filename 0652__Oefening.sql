use aptunes;
drop user if exists student@lacalhost;

CREATE USER student@localhost 
IDENTIFIED BY 'ikbeneenstudent';

GRANT EXECUTE ON procedure Aptunes.GetAlbumDuration TO student@localhost;