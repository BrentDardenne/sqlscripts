use modernways;
select Leeftijdcategorie, avg(p.Loon) as "Gemiddeld loon"
-- Dit is een "afgeleide" tabel ("derived table", ook soms "materialized table")
from 
(select id, floor(Leeftijd / 10) * 10 as Leeftijdcategorie from personeelsleden) as Leeftijdcategorie
inner join personeelsleden as p on P.id  = Leeftijdcategorie.id
group by Leeftijdcategorie having avg(P.Loon)
order by Leeftijdcategorie;