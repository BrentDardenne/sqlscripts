-- Oefening 0629
select distinct e.Studenten_id as Id
from evaluaties as e
inner join studenten as s on e.Studenten_id = s.Id
group by s.Id having avg(e.Cijfer) > (select avg(Cijfer) from evaluaties);