use aptunes;
-- vraag 0625

-- create index MuzikantenIdVoornaamAchternaam
-- on muzikanten(id, Voornaam(9), Familienaam(9));

create index familinaamVoornaamidx on Muzikanten(Familienaam, Voornaam); -- volgorde is belangrijk
-- toont per naam het aantal keer dat iemand met die naam lid is van een groep
-- iemand die lid is van twee groepen, wordt dus twee keer geteld
-- naamgenoten zijn mogelijk en worden samen geteld
select Voornaam, Familienaam, count(Lidmaatschappen.Muzikanten_Id)
from Muzikanten inner join Lidmaatschappen
on Lidmaatschappen.Muzikanten_Id = Muzikanten.Id
group by Familienaam, Voornaam
order by Voornaam, Familienaam; -- verfijnen
-- Old Execution time: 0:00:0.08533980
-- new Execution time: 0:00:0.06508660

-- explain select Voornaam, Familienaam, count(Lidmaatschappen.Muzikanten_Id)
-- from Muzikanten inner join Lidmaatschappen
-- on Lidmaatschappen.Muzikanten_Id = Muzikanten.Id
-- group by Familienaam, Voornaam
-- order by Voornaam, Familienaam;
 