 -- vraag 5.1 0623
 use aptunes;
select count(distinct left (Naam, 2))
from genres;
 
 
 -- index
 create index NaamIndex
 on genres(Naam);
 
 select * from genres;
 
-- niet nodig
-- select count(distinct left (Titel, 42))
-- from liedjes;
 
-- niet nodig
-- create index TitelIndex
-- on liedjes(Titel(42));
 drop index NaamIndex on genres;
 
-- create index TitelIndex
-- on liedjes(Titel(100));
-- drop index TitelIndex on liedjes;
select Titel, Naam
from Liedjesgenres inner join Liedjes
on Liedjesgenres.Liedjes_Id = Liedjes.Id
inner join Genres
on Liedjesgenres.Genres_Id = Genres.Id
where Naam = "Rock"; -- dit is nodig
-- Execution time 0:00:4.50142070
-- new Execution time

explain select Titel, Naam
from Liedjesgenres inner join Liedjes
on Liedjesgenres.Liedjes_Id = Liedjes.Id
inner join Genres
on Liedjesgenres.Genres_Id = Genres.Id;

