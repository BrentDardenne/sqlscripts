USE `aptunes`;
DROP procedure IF EXISTS `AlbumReleases`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `AlbumReleases`()
BEGIN
	declare numberOfAlbums int default 0;
    declare numberOfBands int default 0;
    declare randomAlbumId int default 0;
    declare randomBandId int default 0;
    
    select count(*)
    into numberOfAlbums
    from Albums;
    
    select count(*)
    into numberOfBands
    from Bands;
    
    set randomAlbumid = floor(rand() * numberOfAlbums) + 1;
    set randomBandId = floor(rand() * numberOfBands) + 1;
    
    -- checkt of die waarde al dan niet teogevoed is waarbij niks doen
    if (randomBandId, randomAlbumId) not in (select * from Albumreleases) then
		insert into albumreleases (bands_id, albums_id) values (randomBandId, randomAlbumid);
	end if;
END$$

DELIMITER ;

