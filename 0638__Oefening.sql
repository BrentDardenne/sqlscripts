use ModernWays;

-- Een nieuwe persoon toevoegen aan de tabel Personen
insert into Personen (
   Voornaam, Familienaam, AanspreekTitel
)
values (
   'Hilary', 'Mantel', 'Mevrouw'
);

select * from Personen;

-- nieuwe rij van data/informatie in de kolomen van Boeken steken
insert into Boeken (
   Titel, Stad, Uitgeverij, Verschijningsdatum,
   Herdruk, Commentaar, Categorie, Personen_Id
)
values (
   'Wolf Hall', '', 'Fourth Estate; First Picador Edition First Printing edition',
   '2010', '', 'Goed boek', 'Thriller', 16
);

-- Een lijst ophalen van alle informatie over Boeken
select * from Boeken;
