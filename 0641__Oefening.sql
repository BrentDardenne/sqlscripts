select * from genres;

USE `aptunes`;
DROP procedure IF EXISTS `NumberOfGeneres`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `NumberOfGeneres`(OUT totalGenre tinyint)
BEGIN
	select count(Naam)
    into totalGenre
    from genres;
END$$

DELIMITER ;


call NumberOfGeneres(@totalGenre);
select @totalGenre;
