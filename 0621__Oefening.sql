use aptunes;

-- vraag 1 0619
create index TitelIndex
on albums(Titel(100));

-- vraag 2 0620
create index VoornaamFamilienaamMuzikantenIndex
on muzikanten(Voornaam(45), Familienaam(45));

-- vraag 3 0621
drop index VoornaamFamilienaamMuzikantenIndex on muzikanten;
 