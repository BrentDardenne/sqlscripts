USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleases`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumReleases`(in extraRelease int)
BEGIN
	declare counter int default 0;
    declare success bool;
    repeat
		call MockAlbumReleaseWithSuccess(success);
        if success = 1 then
			set counter = counter + 1;
		end if;
		until counter >= extraRelease
    end repeat;
END$$

DELIMITER ;



