use modernways;

-- nieuw persoon toevoegen in de kolomen van Voornaam en Achternaam in de tabel Personen
insert into Personen (Voornaam, Familienaam) values
('Jean-Paul','Sartre' );


use ModernWays;
-- De lijst van Boeken aanvullen met data Voor de laatste id gaan we wet een sql statment zoeken wat de id is van die persoon op de Voornaam en Achternaam
insert into Boeken (
   Titel,
   Stad,
   Verschijningsdatum,
   Commentaar,
   Categorie,
   Personen_Id
)
values (
   'De Woorden',
   'Antwerpen',
   '1962',
   'Een zeer mooi boek.',
   'Roman',
   (select Id from Personen where
       Familienaam = 'Sartre' and Voornaam = 'Jean-Paul'));
       
select * from Personen;       