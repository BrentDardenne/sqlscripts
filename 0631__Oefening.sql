-- Oefening 5 0631
-- Oefening 5 0631
select distinct Voornaam
from studenten
where Voornaam in (select Voornaam -- als voornaam student ook in voornaam personeelsleden
				   from personeelsleden
                   where Voornaam in (select Voornaam
									  from directieleden));