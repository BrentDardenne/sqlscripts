-- verwijderen databbase
DROP DATABASE IF EXISTS ModernWays; 
-- database crieeren
CREATE DATABASE ModernWays;

use Modernways;

create table Nummer(
Titel varchar(100) character set utf8mb4 not null,
Artiest varchar(100) character set utf8mb4 not null,
Genre varchar(50),
Jaar char(4)
);

create table Huisdieren (
Naam varchar(100) character set utf8mb4 not null,
Leeftijd smallint not null,
Soort varchar(50) not null
);

-- verander naam van nummers naar liedjes
rename table Nummer to Liedjes;

-- Kolom Album toevoegen aan liedjes
alter table Liedjes add column Album varchar(100) character set utf8mb4;

-- verwijder kolom genre
alter table Liedjes drop column Album;





