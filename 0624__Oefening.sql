use aptunes;

-- vraag 5.2 0624
create index bandsindex
on bands(Id, Naam(100));

create index TitelIdx on Liedjes(Titel);

-- toont combinaties van liedjes en bands
-- doet dit enkel voor liedjestitels die beginnen met 'A'
-- gaat van kort naar lang
SELECT Titel, Naam, Lengte FROM Liedjes
inner join Bands
on Liedjes.Bands_Id = Bands.Id
where Titel like 'A%' -- altijd verfijnen
order by Lengte;
-- Old Execution time 0:00:0.13177840
-- new Execution time 0:00:0.13262610

explain SELECT Titel, Naam, Lengte FROM Liedjes
inner join Bands
on Liedjes.Bands_Id = Bands.Id
where Titel like 'A%'
order by Lengte;

 