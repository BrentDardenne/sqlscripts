create database Hogeschool;
use Hogeschool;

create table Lector(
Personeelsnr int auto_increment primary key not null,
Voornaam varchar(100) not null,
Famillienaam varchar(100)
);

create table Vak(
naamId_vak varchar(100) primary key not null
);

create table Geeft(
Personeelsnr_id int,
naamId_id varchar(100),
constraint fk_Lector_Vak foreign key(Personeelsnr_id) references Lector(Personeelsnr),
constraint fk_Geeft_Vak foreign key(naamId_id) references Vak(naamId_vak)
);

create table Opleiding(
NaamId_Opleiding varchar(100) primary key not null
);

create table OpleidingsOnderdeel(
semester tinyint not null,
naamId_id_vak varchar(100),
naamId_id_opleiding varchar(100),
constraint fk_OpleidingsOnderdeel_Vak foreign key(naamId_id_vak) references Vak(naamId_vak),
constraint fk_OpleidingsOnderdeel_Opleiding foreign key(naamId_id_opleiding) references Opleiding(NaamId_Opleiding)
);

create table Student(
Studentennr_Student int auto_increment primary key not null,
voornaam varchar(100) not null,
Famillienaam varchar(100) not null,
Semester tinyint not null,
NaamId_id_Opleiding varchar(100),
constraint fk_Student_Opleiding foreign key(NaamId_id_Opleiding) references Opleiding(NaamId_Opleiding)
);